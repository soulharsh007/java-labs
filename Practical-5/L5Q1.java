import java.util.Scanner;

public class L5Q1 {
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    System.out.print("Enter binary number: ");
    String binary = scan.nextLine();
    scan.close();
    try {
      bin2Dec(binary);
    } catch (NumberFormatException e) {
      System.err.println(e);
    }
  }

  private static void bin2Dec(String binary) throws NumberFormatException {
    int value = 0;
    int length = binary.length();
    for (int i = 0; i < length; i++) {
      int bin = Integer.parseInt("" + binary.charAt(i));
      if (bin != 0 && bin != 1) {
        throw new NumberFormatException(
            "Expected a binary value, but got: " + bin + " at index: " + i + " of the input string");
      }
      value += bin * Math.pow(2, length - (i + 1));
    }
    System.out.println("The decimal number is: " + value);
  }
}
