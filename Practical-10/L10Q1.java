public class L10Q1 extends Thread {
  String msg;
  int interval;

  public L10Q1(String msg, int interval) {
    this.msg = msg;
    this.interval = interval * 1000;
  }

  public static void main(String[] args) {
    L10Q1 morning = new L10Q1("Good Morning", 1);
    L10Q1 hello = new L10Q1("Hello", 2);
    L10Q1 welcome = new L10Q1("Welcome", 3);
    morning.start();
    hello.start();
    welcome.start();
  }

  public void run() {
    while (true) {
      System.out.println(msg);
      try {
        sleep(interval);
      } catch (InterruptedException e) {
        System.err.println("Thread interrupted: " + e);
      }
    }
  }
}
