public class L10Q2 extends Thread {
  public static void main(String[] args) {
    L10Q2 t1 = new L10Q2();
    t1.start();
    System.out.println("Is child thread running? checking using <Thread>.isAlive: " + t1.isAlive());
    if (t1.isAlive()) {
      try {
        System.out.println("Child thread is running, joining using <Thread>.join... ");
        t1.join();
      } catch (InterruptedException e) {
        System.err.println("Main thread interrupted: " + e);
      }
    }
    System.out.println("Child thread exited");
    System.out.println("Main thread is exiting...");
  }

  public void run() {
    System.out.println("Child thread running...");
    try {
      sleep(3000);
    } catch (InterruptedException e) {
      System.err.println("Thread interrupted: " + e);
    }
    System.out.println("Child thread is exiting...");
  }
}
