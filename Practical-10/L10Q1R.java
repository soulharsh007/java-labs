public class L10Q1R implements Runnable {
  String msg;
  int interval;
  Thread thread;

  public L10Q1R(String msg, int interval) {
    this.msg = msg;
    this.interval = interval * 1000;
    this.thread = new Thread(this);
    this.thread.start();
  }

  public static void main(String[] args) {
    new L10Q1R("Good Morning", 1);
    new L10Q1R("Hello", 2);
    new L10Q1R("Welcome", 3);
  }

  public void run() {
    while (true) {
      System.out.println(msg);
      try {
        Thread.sleep(interval);
      } catch (InterruptedException e) {
        System.err.println("Thread interrupted: " + e);
      }
    }
  }
}
