public class AddAmount {
  int amount = 50;

  AddAmount() {
    this.amount = 50;
  }

  AddAmount(int amount) {
    this.amount += amount;
  }

  public static void main(String[] args) {
    AddAmount bank1 = new AddAmount();
    System.out.println("Initial Value: " + bank1.amount);
    AddAmount bank2 = new AddAmount(500);
    System.out.println("Updated Value: " + bank2.amount);
  }
}
