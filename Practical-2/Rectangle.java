public class Rectangle {
  double width;
  double length;
  double area;
  String color;

  public static void main(String[] args) {
    Rectangle a = new Rectangle(15, 20, "Red");
    Rectangle b = new Rectangle(15, 20, "Red");
    if (a.color.equals(b.color) && a.area == b.area) {
      System.out.println("Matching Rectangles");
    } else {
      System.out.println("Non matching Rectangle");
    }
  }

  public Rectangle(double width, double length, String color) {
    this.width = width;
    this.length = length;
    this.area = length * width;
    this.color = color;
  }

  public void set_width(double width) {
    this.width = width;
  }

  public void set_length(double length) {
    this.length = length;
  }

  public double find_area() {
    return area;
  }

  public void set_color(String color) {
    this.color = color;
  }
}
