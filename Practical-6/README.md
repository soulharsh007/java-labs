# Build instructions

Building / compiling the java files in this section would require JavaFX to be installed on your system and, you need to explicitly include it by passing arguments to java and javac

## Installation guide

This guide is applicable for Arch Linux based distributions ONLY, you would have to modify this according to your distribution.

Start by installing the following packages from the "extra" repository:

- java11-openjfx
- jre11-openjdk
- jre11-openjdk-headless
- jdk11-openjdk

Installation command would look like:

```sh
sudo pacman -S java11-openjfx jre11-openjdk jre11-openjdk-headless jdk11-openjdk
```

if you have multiple versions of JDK installed, you will have to run this command to use JRE11 as default:

```sh
sudo archlinux-java set java-11-openjdk
```

## Compilation guide

To compile the java files, you must include the javafx modules during the compile-time and run-time

commands to compile would be:

For Arch Linux:

```sh
javac --add-modules javafx.controls,javafx.fxml --module-path /usr/lib/jvm/java-11-openjfx/lib L6Q1.java
```

For any other distribution or operating system:

```sh
javac --add-modules javafx.controls,javafx.fxml --module-path <PATH-JFX-LIB> L6Q1.java
```

commands to run the compiled class file would be:

For Arch Linux:

```sh
java --add-modules javafx.controls,javafx.fxml --module-path /usr/lib/jvm/java-11-openjfx/lib L6Q1
```

For any other distribution or operating system:

```sh
java --add-modules javafx.controls,javafx.fxml --module-path <PATH-JFX-LIB> L6Q1
```
