import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

public class L6Q1 extends Application {

  public void start(Stage stage) {
    Circle c = new Circle(500, 500, 200, Color.WHITE);
    c.setFill(Color.BLUE);
    Pane pane = new Pane(c);
    stage.setScene(new Scene(pane, 1000, 1000));
    pane.setOnMousePressed(e -> c.setFill(Color.RED));
    pane.setOnMouseReleased(e -> c.setFill(Color.BLUE));
    stage.setTitle("JavaFX - Lab 6 Question 1");
    stage.show();
  }

  public static void main(String[] args) {
    Application.launch(args);
  }
}
