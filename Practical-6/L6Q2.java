import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

public class L6Q2 extends Application {

  public void start(Stage stage) {
    Circle c = new Circle(500, 500, 200, Color.RED);
    Pane pane = new Pane(c);
    pane.setOnKeyPressed(e -> {
      switch (e.getCode()) {
        case UP:
          c.setCenterY(c.getCenterY() - 10);
          break;
        case DOWN:
          c.setCenterY(c.getCenterY() + 10);
          break;
        case LEFT:
          c.setCenterX(c.getCenterX() - 10);
          break;
        case RIGHT:
          c.setCenterX(c.getCenterX() + 10);
          break;
      }
    });
    stage.setScene(new Scene(pane, 1000, 1000));
    stage.setTitle("JavaFX - Lab 6 Question 2");
    stage.show();
    pane.requestFocus();
  }

  public static void main(String[] args) {
    Application.launch(args);
  }
}
