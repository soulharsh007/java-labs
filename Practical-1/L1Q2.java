public class L1Q2 {
  public static void main(String[] args) {
    if (args.length != 2) {
      System.out.println("Expected two arguments in the following manner: [weight in pounds] [height in inches]");
    } else {
      double weight = Double.parseDouble(args[0]) * 0.45359237;
      double height = Double.parseDouble(args[1]) * 0.0254;
      System.out.println("Your BMI is: " + weight / (height * height));
    }
  }
}
