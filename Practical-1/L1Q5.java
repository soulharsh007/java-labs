import java.util.Scanner;

public class L1Q5 {
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    int[] data = new int[10];
    int i;
    for (i = 0; i < 10; i++) {
      System.out.print("Enter the number at index " + (i + 1) + ": ");
      data[i] = scan.nextInt();
    }
    for (i = 0; i < 10; i++) {
      System.out.println("Reversed number at index " + (i + 1) + ": " + reverse(data[i]));
    }
    scan.close();
  }

  public static String reverse(int number) {
    String reversed = "";
    while (number != 0) {
      int split = number % 10;
      number /= 10;
      reversed += split;
    }
    return reversed;
  }
}
