public class L1Q3 {
  public static void main(String[] args) {
    if (args.length != 1) {
      System.out.println("Expected one argument in the following manner: [number to find factors for]");
    } else {
      int number = Integer.parseInt(args[0]);
      if (number == 0) {
        System.out.println(1);
      } else {
        while (number != 1) {
          int i;
          for (i = 2; i != number; i++) {
            if (number % i == 0) {
              break;
            }
          }
          number /= i;
          if (number == 1) {
            System.out.println(i);
          } else {
            System.out.print(i + ", ");
          }
        }
      }
    }
  }
}
