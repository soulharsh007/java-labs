import java.io.BufferedReader;
import java.io.InputStreamReader;

public class L1Q4 {
  public static void main(String[] args) {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    String rawData = "";
    System.out.print("Enter 1st number: ");
    try {
      rawData = reader.readLine();
    } catch (Exception e) {
    }
    int n1 = Integer.parseInt(rawData);
    System.out.print("Enter 2nd number: ");
    try {
      rawData = reader.readLine();
    } catch (Exception e) {
    }
    int n2 = Integer.parseInt(rawData);
    System.out.println("The Greatest Common Divisor (GCD) for " + n1 + " and " + n2 + " is: " + gcd(n1, n2));
  }

  public static int gcd(int num1, int num2) {
    int i;
    for (i = num1 < num2 ? num2 : num1; i > 1; i--) {
      if (num1 % i == 0 && num2 % i == 0) {
        break;
      }
    }
    return i;
  }
}
