public class Employee {
  int ID;
  String name;
  String phoneNumber;

  public static void main(String[] args) {
    FullTime emp = new FullTime(404, "James Bond", "1200102070", 150);
    emp.print();
  }

  Employee(int ID, String name, String phoneNumber) {
    this.ID = ID;
    this.name = name;
    this.phoneNumber = phoneNumber;
  }
}

public class PartTime extends Employee {
  double salary;

  PartTime(int ID, String name, String phoneNumber, double salary) {
    super(ID, name, phoneNumber);
    this.salary = salary;
  }

  void print() {
    System.out.println("ID: " + this.ID);
    System.out.println("Name: " + this.name);
    System.out.println("Phone Number: " + this.phoneNumber);
    System.out.println("Salary: " + this.salary);
  }
}

public class FullTime extends Employee {
  double salary;

  FullTime(int ID, String name, String phoneNumber, double salary) {
    super(ID, name, phoneNumber);
    this.salary = salary;
  }

  void print() {
    System.out.println("ID: " + this.ID);
    System.out.println("Name: " + this.name);
    System.out.println("Phone Number: " + this.phoneNumber);
    System.out.println("Salary: " + this.salary);
  }
}
