abstract class Data {
  int id;

  public abstract void setID(int id);

  void print() {
    System.out.println("ID: " + this.id);
  }
}

public class L4Q2 extends Data {
  public static void main(String[] args) {
    L4Q2 test = new L4Q2();
    test.setID(4);
    test.print();
  }

  public void setID(int id) {
    this.id = id;
  }
}
