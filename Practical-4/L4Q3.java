interface Data {
  public void setID(int id);

  public void print();
}

public class L4Q3 implements Data {
  int id;

  public static void main(String[] args) {
    L4Q3 test = new L4Q3();
    test.setID(4);
    test.print();
  }

  public void setID(int id) {
    this.id = id;
  }

  public void print() {
    System.out.println("ID: " + this.id);
  }
}
