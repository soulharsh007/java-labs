public class L3Q3 {
  public static void main(String[] args) {
    DailyWorker dw = new DailyWorker("James Bond", 1500);
    SalariedWorker sw = new SalariedWorker("James Bond", 1500);
    System.out.println("Daily worker's salary for 44 hours a week: " + dw.comPay(44));
    System.out.println("Salaried worker's salary for 44 hours a week: " + sw.comPay(44));
  }
}

public class Worker {
  String name;
  float salaryRate;

  public Worker(String name, float salaryRate) {
    this.name = name;
    this.salaryRate = salaryRate;
  }
}

public class SalariedWorker extends Worker {
  SalariedWorker(String name, float salaryRate) {
    super(name, salaryRate);
  }

  float comPay(int hours) {
    return this.salaryRate * 40;
  }
}

public class DailyWorker extends Worker {
  DailyWorker(String name, float salaryRate) {
    super(name, salaryRate);
  }

  float comPay(int hours) {
    return this.salaryRate * hours;
  }
}
