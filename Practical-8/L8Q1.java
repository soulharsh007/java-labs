import java.io.BufferedReader;
import java.io.BufferedWriter;

import java.io.FileReader;
import java.io.FileWriter;
import java.util.Random;

public class L8Q1 {
  public static void main(String[] args) {
    try {
      FileWriter fOut = new FileWriter("123.txt", true);
      BufferedWriter out = new BufferedWriter(fOut);
      System.out.println("Adding 150 numbers to 123.txt...");
      System.out.println("=====================================");
      Random random = new Random();
      for (int i = 0; i < 150; i++) {
        out.append(random.nextInt(1000) + " ");
      }
      out.close();
      FileReader fIn = new FileReader("123.txt");
      BufferedReader in = new BufferedReader(fIn);
      System.out.println("The new contents of the file are:");
      System.out.println("=====================================");
      System.out.println(in.readLine());
      in.close();
    } catch (Exception e) {
      System.err.println(e);
    }
  }
}
