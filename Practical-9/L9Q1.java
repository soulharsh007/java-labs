import java.util.Scanner;

public class L9Q1 {
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    System.out.print("Enter a number to find a factorial for: ");
    int number = scan.nextInt();
    scan.close();
    System.out.println("Factorial of " + number + " is: " + factorial(number));
  }

  public static int factorial(int number) {
    if (number > 1) {
      return number * factorial(number - 1);
    }
    return 1;
  }
}
