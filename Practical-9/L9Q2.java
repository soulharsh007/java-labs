import java.util.Scanner;

public class L9Q2 {
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    Integer[][] intArray = new Integer[2][2];
    System.out.println("Created a 2d integer array of dimensions: [2][2]...");
    for (int i = 0; i < intArray.length; i++) {
      for (int j = 0; j < intArray.length; j++) {
        System.out.print("Enter element at array index: [" + i + "][" + j + "]: ");
        intArray[i][j] = scan.nextInt();
      }
    }
    System.out.println("Minimum value: " + findMin(intArray));
    Character[][] charArray = new Character[2][2];
    System.out.println("Created a 2d char array of dimensions: [2][2]...");
    for (int i = 0; i < charArray.length; i++) {
      for (int j = 0; j < charArray.length; j++) {
        System.out.print("Enter element at array index: [" + i + "][" + j + "]: ");
        charArray[i][j] = scan.next().charAt(0);
      }
    }
    System.out.println("Minimum value: " + findMin(charArray));
    scan.close();
  }

  public static <T extends Comparable<T>> T findMin(T[][] array) {
    T min = array[0][0];
    for (int i = 0; i < array.length; i++) {
      for (int j = 0; j < array.length; j++) {
        if (array[i][j].compareTo(min) < 0) {
          min = array[i][j];
        }
      }
    }
    return min;
  }
}
