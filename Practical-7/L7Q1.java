import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.paint.Color;

public class L7Q1 extends Application {
  public void start(Stage primaryStage) {
    Text text = new Text(200, 20, "Hello World!");
    text.setTextAlignment(TextAlignment.CENTER);
    HBox buttonPane = new HBox(20);
    Button left = new Button("<= Left");
    Button right = new Button("Right =>");
    buttonPane.getChildren().addAll(left, right);
    buttonPane.setAlignment(Pos.CENTER);
    VBox pane = new VBox(50);
    HBox radioButtonPane = new HBox(20);
    RadioButton redButton = new RadioButton("Red");
    RadioButton greenButton = new RadioButton("Green");
    RadioButton yellowButton = new RadioButton("Yellow");
    RadioButton blueButton = new RadioButton("Blue");
    radioButtonPane.getChildren().addAll(redButton, yellowButton, greenButton, blueButton);
    radioButtonPane.setAlignment(Pos.CENTER);
    ToggleGroup group = new ToggleGroup();
    redButton.setToggleGroup(group);
    yellowButton.setToggleGroup(group);
    greenButton.setToggleGroup(group);
    blueButton.setToggleGroup(group);
    Pane textPane = new Pane(text);
    pane.getChildren().addAll(textPane, buttonPane, radioButtonPane);
    left.setOnAction(e -> text.setX(text.getX() - 10));
    right.setOnAction(e -> text.setX(text.getX() + 10));
    redButton.setOnAction(e -> {
      if (redButton.isSelected()) {
        text.setFill(Color.RED);
      }
    });
    yellowButton.setOnAction(e -> {
      if (yellowButton.isSelected()) {
        text.setFill(Color.YELLOW);
      }
    });
    greenButton.setOnAction(e -> {
      if (greenButton.isSelected()) {
        text.setFill(Color.GREEN);
      }
    });
    blueButton.setOnAction(e -> {
      if (blueButton.isSelected()) {
        text.setFill(Color.BLUE);
      }
    });
    Scene scene = new Scene(pane, 500, 500);
    primaryStage.setTitle("JavaFX - Lab 7 Question 1");
    primaryStage.setScene(scene);
    primaryStage.show();
  }
}
